all: dwm dwmblocks dmenu st dotfiles scripts
.PHONY: install_all

recompile: dwm dwmblocks dmenu st dotfiles scripts
.PHONY: recompile

dwm:
	sudo cp dwm/dwm-6.2/config.def.h dwm/dwm-6.2/config.h
	sudo make -C dwm/dwm-6.2 clean install
	sudo mkdir -p /usr/share/xsessions/
	sudo cp "dwm/dwm.desktop" "/usr/share/xsessions/dwm.desktop"
.PHONY: dwm

dwmblocks:
	sudo cp dwmblocks/dwmblocks/blocks.def.h dwmblocks/dwmblocks/blocks.h
	sudo make -C dwmblocks/dwmblocks clean install
.PHONY: dwmblocks

dmenu:
	sudo cp dmenu/dmenu-4.9/config.def.h dmenu/dmenu-4.9/config.h
	sudo make -C dmenu/dmenu-4.9 clean install
.PHONY: dmenu

st:
	sudo cp st/st-0.8.3/config.def.h st/st-0.8.3/config.h
	sudo make -C st/st-0.8.3 clean install
.PHONY: st

dotfiles:
	rsync -avz0 dotfiles/USER/ ~/
.PHONY: dotfiles

scripts:
	mkdir -p "${HOME}/.local/bin/"
	cp scripts/* ${HOME}/.local/bin/
.PHONY: scripts
