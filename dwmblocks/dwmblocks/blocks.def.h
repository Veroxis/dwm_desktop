const unsigned short second = 1;
const unsigned short minute = 60;
const unsigned short hour = 3600;

//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/            /*Update Interval*/      /*Update Signal*/
	/* {"", "echo -e ' '",                0,                       0}, */
	/* {"", "checkupdates.sh",            (30 * minute),           10}, */
	/* {"", "notifications.sh",           0,                       11}, */
	{"", "volume.sh",                  ( 1 * second),           12},
	{"", "calendar.sh",                (15 * second),           13},
	/* {"", "battery.sh",                 (1 * minute),            14}, */
	/* {"", "echo -e ' '",                0,                       0}, */
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 3;
