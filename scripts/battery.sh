#!/bin/sh

# battery.sh

# Signal Handling
case ${DWMBLOCKS_SIGNAL} in
	# Left Click
	1) ;;
	# Middle Click
	2) notify-send "battery.sh" ;;
	# Right Click
	3) ;;
	# Scroll Up
	4) ;;
	# Scroll Down
	5) ;;
	# Shift + Left Click
	6) setsid -f alacritty -e "${EDITOR}" "$0" ;;
esac

ICON_WARN=""
ICON_BATTERY=""
ICON_CHARGER=""
ICON_UNKNOWN=""
ICON_FULL=""

BATTERY_COUNTER=$(ls -1q /sys/class/power_supply/BAT* 2>/dev/null | wc -l)

if [ "${BATTERY_COUNTER}" -eq 0 ]; then
	echo -e "${ICON_CHARGER} "
else
	# Loop through all attached batteries.
	for battery in /sys/class/power_supply/BAT?
	do
		# Get its remaining capacity and charge status.
		capacity=$(cat "${battery}"/capacity 2>/dev/null) || break
		status=$(sed "s/[Dd]ischarging/${ICON_BATTERY}/;s/[Nn]ot charging/${ICON_BATTERY}/;s/[Cc]harging/${ICON_CHARGER}/;s/[Uu]nknown/${ICON_UNKNOWN}/;s/[Ff]ull/${ICON_FULL}/" "${battery}"/status)

		# If it is discharging and 25% or less, we will add a ❗ as a warning.
		if [ "$capacity" -le 25 ] && [ "$status" = "${ICON_BATTERY}" ]; then
			warn="${ICON_WARN}"
		else
			warn=""
		fi

		printf "%s %s% s%% " "$status" "$warn" "$capacity"
		unset warn
	done | sed 's/ *$//'
fi
