#!/bin/sh

# calendar.sh

# Signal Handling
case ${DWMBLOCKS_SIGNAL} in
	# Left Click
	1) setsid -f alacritty -e "calcurse" ;;
	# Middle Click
	2) notify-send "calendar.sh" ;;
	# Right Click
	3) ;;
	# Scroll Up
	4) ;;
	# Scroll Down
	5) ;;
	# Shift + Left Click
	6) setsid -f alacritty -e "${EDITOR}" "$0" ;;
esac

# stdout for taskbar
echo -e " $(date +" %a, %d. %b %H:%M ")"
