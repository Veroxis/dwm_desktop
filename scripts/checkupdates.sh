#!/bin/sh

# checkupdates.sh

AMOUNT_UPDATES_AVAILABLE=$(checkupdates | wc -l)

# Signal Handling
case ${DWMBLOCKS_SIGNAL} in
	# Left Click
	1) ;;
	# Middle Click
	2) notify-send "checkupdates.sh" ;;
	# Right Click
	3) ;;
	# Scroll Up
	4) ;;
	# Scroll Down
	5) ;;
	# Shift + Left Click
	6) setsid -f alacritty -e "${EDITOR}" "$0" ;;
esac

if [ "${AMOUNT_UPDATES_AVAILABLE}" -eq 0 ]; then
	printf ""
elif [ "${AMOUNT_UPDATES_AVAILABLE}" -eq 1 ]; then
	printf " %s update" "${AMOUNT_UPDATES_AVAILABLE}"
else
	printf " %s updates" "${AMOUNT_UPDATES_AVAILABLE}"
fi
