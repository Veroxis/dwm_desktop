#!/bin/sh

# volume.sh

# Signal Handling
case ${DWMBLOCKS_SIGNAL} in
	# Left Click
	1) pamixer -t ;;
	# Middle Click
	2) notify-send "volume.sh" ;;
	# Right Click
	3) setsid -f pavucontrol ;;
	# Scroll Up
	4) pamixer -i 5 ;;
	# Scroll Down
	5) pamixer -d 5 ;;
	# Shift + Left Click
	6) setsid -f alacritty -e "${EDITOR}" "$0" ;;
esac

CURRENT_VOLUME="$(pamixer --get-volume)"
MUTED=$(pamixer --get-mute)

if [ ${MUTED} = true ]; then
	ICON=""
elif [ "${CURRENT_VOLUME}" -eq "0" ]; then
	ICON=""
elif [ "${CURRENT_VOLUME}" -lt "20" ]; then
	ICON=""
else
	ICON=""
fi

echo -e "${ICON} ${CURRENT_VOLUME}%"
